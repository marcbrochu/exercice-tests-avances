
import java.util.Objects;


public class Argent {
    private final String devise; // abbreviation ISO (USD, CAD, JPY, etc.)
    private final double montant; // btw, des montants monetaires ne devraient jamais etre en double
    
    public Argent (double montant, String devise) {
        this.montant = montant;
        this.devise = devise;
    }

    public String getDevise () {
        return devise;
    }
    
    public double getMontant () {
        return montant;
    }
    
    public boolean estNegatif () {
        return montant < 0.0;
    }

    public Argent ajouter (Argent a) {
        // remarquez que cette methode ne modifie jamais l'objet argent recu en parametre;
        // au contraire, elle en retourne toujours un nouveau, dont la valeur reflete l'addition
        if (!estCompatible(a))
            throw new RuntimeException("Devises incompatibles");
        return new Argent(montant + a.getMontant(), devise);
    }
    
    public Argent retirer (Argent a) {
        if (!estCompatible(a))
            throw new RuntimeException("Devises incompatibles");
        return new Argent(montant - a.getMontant(), devise);
    }
    
    private boolean estCompatible (Argent a) {
        return devise.equals(a.getDevise());
    }

    @Override
    public boolean equals (Object autreObjet) {
        if (autreObjet instanceof Argent) {
            Argent autreArgent = (Argent) autreObjet;
            return estCompatible(autreArgent) && montant == autreArgent.getMontant();
        }
        return false;
    }

    @Override
    public int hashCode () {
        int hash = 3;
        hash = (int) (37 * hash + this.montant);
        hash = 37 * hash + Objects.hashCode(this.devise);
        return hash;
    }
}
