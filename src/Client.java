
public class Client extends Personne {
    
    private static int nbClients = 0;
    private int noClient;

    public Client (String nom, String prenom, boolean genre) {
        super(nom, prenom, genre);
        noClient = ++nbClients;
    }
    
    public int getNoClient () {
        return noClient;
    }
    
    public String getIdentifiant () {
        String nom = getNom().toUpperCase();
        if (nom.length() >= 4)
            return nom.substring(0, 4) + noClient;
        else
            return nom + noClient;
    }

    @Override
    public String getNomFormel () {
        return super.getNomFormel() + " (#" + noClient + ")";
    }
}
