
public abstract class CompteBancaire {
    protected Client proprietaire;
    protected Argent solde;

    public CompteBancaire (Client proprietaire, String devise) {
        this.proprietaire = proprietaire;
        this.solde = new Argent(0.0, devise);
    }
    
    public Client getProprietaire () {
        return proprietaire;
    }

    public String getDevise () {
        return solde.getDevise();
    }

    public double getSolde () {
        return solde.getMontant();
    }
    
    public String imprimerSolde () {
        return getDevise() + " " + getSolde();
    }
    
    public void deposerArgent (Argent montantDepot) {
        if (montantDepot.estNegatif())
            throw new IllegalArgumentException("Le montant d'argent depose ne peut etre negatif");
        
        Argent fraisDepot = calculerFrais(montantDepot);
        if (montantDepot.getMontant() <= fraisDepot.getMontant())
            throw new IllegalArgumentException("Le montant d'argent depose ne couvre pas les frais");
        
        solde = solde.retirer(fraisDepot).ajouter(montantDepot);
    }
    
    protected abstract Argent calculerFrais (Argent montantOperation);

    public void retirerArgent (Argent montantRetrait) 
            throws SoldeInsuffisantException {
        if (montantRetrait.estNegatif())
            throw new IllegalArgumentException("Le montant d'argent retire ne peut etre negatif");
        
        Argent soldeApresFrais = solde.retirer(calculerFrais(montantRetrait));
        if (soldeApresFrais.getMontant() < montantRetrait.getMontant())
            throw new SoldeInsuffisantException("Solde insuffisant pour le retrait demande");
        
        solde = soldeApresFrais.retirer(montantRetrait);
    }

    public void transfererArgentA (Argent montantTransfert, CompteBancaire destinataire) 
            throws SoldeInsuffisantException {
        Argent soldeInitial = solde;
        try {
            retirerArgent(montantTransfert);
            destinataire.deposerArgent(montantTransfert);
        } 
        catch (Exception e) {
            solde = soldeInitial;
            throw new SoldeInsuffisantException("Le transfert d'argent n'a pu etre complete");
        }
    }
    
    // Classe d'exception dediee
    public static class SoldeInsuffisantException extends Exception {
        public SoldeInsuffisantException (String message) {
            super(message);
        }
    }
    
}
