
public class Personne {
    public static final boolean FEMME = true;
    public static final boolean HOMME = false;

    private String nom, prenom;
    private boolean genre;

    public Personne (String nom, String prenom, boolean genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.genre = genre;
    }
    
    public String getNom () {
        return nom;
    }

    public String getPrenom () {
        return prenom;
    }
    
    public String getNomComplet () {
        return prenom + " " + nom;
    }
    
    public String getNomFormel () {
        return (genre ? "Mme " : "M. ") + nom;
    }
}
